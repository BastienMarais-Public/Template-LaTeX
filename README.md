# README 

## Changer la page de garde

Pour changer la page de garde il faut mettre votre pdf a la place de front/front.pdf

## Ajouter un chapitre 

Pour ajouter une chapitre il faut seulement :
 1. Créer un fichier body/chapter_X.tex (X étant son numero)
 2. Faire le include{body/chapter_X} dans le main.tex
 3. Écrire votre chapitre dans le fichier body/chapter_X

## Ajouter des packages ou des configs spécifique au projet

Pour ajouter des packages ou de la conf -> Ajouter tout ça dans lib/specific.sty
 
## Ajouter des images, fichiers et du code

Chemins où mettre mes fichers externes :
1. Images  ->  assets/img/monImage.png
2. Fichier ->  assets/file/monFichier.pdf
3. Code    ->  assets/src/myScript.py (\lstinputlisting[language=Python]{assets/src/myScript.py})


Bon courage pour le rapport.